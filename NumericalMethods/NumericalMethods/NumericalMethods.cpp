﻿#include <iostream>
#include <algorithm>

using namespace std;

// https://github.com/0razzu/na1

/*
#include <iostream>

using namespace std;

#define N 4

// This function multiplies
// mat1[][] and mat2[][], and
// stores the result in res[][]
void multiply(int mat1[][N],
			  int mat2[][N],
			  int res[][N])
{
	int i, j, k;
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			res[i][j] = 0;
			for (k = 0; k < N; k++)
				res[i][j] += mat1[i][k] * mat2[k][j];
		}
	}
}

// Driver Code
int main()
{
	int i, j;
	int res[N][N]; // To store result
	int mat1[N][N] = { { 1, 1, 1, 1 },
					   { 2, 2, 2, 2 },
					   { 3, 3, 3, 3 },
					   { 4, 4, 4, 4 } };

	int mat2[N][N] = { { 1, 1, 1, 1 },
					   { 2, 2, 2, 2 },
					   { 3, 3, 3, 3 },
					   { 4, 4, 4, 4 } };

	multiply(mat1, mat2, res);

	cout << "Result matrix is \n";
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++)
			cout << res[i][j] << " ";
		cout << "\n";
	}

	return 0;
}

*/

double** multiply(double **mat1, double **mat2, int N) {
	int i, j, k;
	double **res = new double*[N];
	for (int i = 0; i < N; i++) {
		res[i] = new double[N];
	}

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			res[i][j] = 0;
			for (k = 0; k < N; k++)
				res[i][j] += mat1[i][k] * mat2[k][j];
		}
	}
	return res;
}

double** Q_old(double **a, int N) {
	double **q = new double*[N];
	for (int i = 0; i < N; i++) {
		q[i] = new double[N];
	}

	double s;
	double c;
	/*
	double z; // z не может = 0
	double l;
	*/
	double _a_i;
	double _a_j;

	// a[][j] = a_j[i] - Это текущий столбец из матрицы
	// a_j[i]*a_j[i] + a_j[j]*a_j[j] != 0  
	for (int j = 0; j < N; j++) { // j - столбец
		for (int i = j + 1; i < N; i++) { // i - строка
			//if (i > j) {
				/*
				z = max(abs(a[i][j]), abs(a[j][j]));
				_a_i = a[i][j] / z;
				_a_j = a[j][j] / z;

				l = min(abs(a[i][j]), abs(a[j][j]));
				*/
			_a_i = a[i][j]; // - i-ый элемент в столбце векторе a (j-го столбца)
			_a_j = a[j][j]; // - j-ый элемент в столбце векторе a (j-го столбца)

			if (((_a_i * _a_i) + (_a_j * _a_j)) == 0) { // Чтобы небыло деления на 0
				continue;
			}
			double det = sqrt((_a_i * _a_i) + (_a_j * _a_j));
			s = -(_a_i / det);
			c = _a_j / det;


			cout << "++++++++++++++++++++++++++++++++++++++++" << endl;
			cout << s * s + c * c << endl;
			cout << "++++++++++++++++++++++++++++++++++++++++" << endl;
			/*
			// Строим матрицу Q (для домножения на матрицу A слева(!), чтобы занулить A[i][j] элемент по гл. диагональю)
			for (int p = 0; p < N; p++) { // p - строка
				for (int k = 0; k < N; k++) { // k - столбец
					q[p][k] = 0;
					if (p == k) { // диагональ
						q[p][k] = 1;
					}
					if ((p == j && k == j) || (p == i && k == i)) { // верхний левый и нижний правый c
						q[p][k] = c;
					}
					else if (p == i && k == j) { // нижний левый s
						q[p][k] = s;
					}
					else if (p == j && k == i) { // правый верхний s
						q[p][k] = -s;
					}
				}
			}
			*/

			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++)
					cout << a[i][j] << " ";
				cout << "\n";
			}
			cout << "----------------------------------------" << endl;
			/*
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++)
					cout << q[i][j] << " ";
				cout << "\n";
			}
			cout << "----------------------------------------" << endl;
			*/
			// Q у нас есть (массив q), A - входная матрица (массив a) => считаем Q*A, где в результате должен занулиться a[i][j] элемент
			//a = multiply(q, a, N);

			for (int jj = j; jj < N; jj++) {
				double buf = a[j][jj];
				a[j][jj] = c * buf - s * a[i][jj];
				a[i][jj] = s * buf + c * a[i][jj];
			}


			//}
		}
	}

	return a;
};

/*
void de_up_triangularize(double a[N][N], unsigned col_transp[N], unsigned str_transp[N]) {
	for (unsigned j = N - 2; j >= 0 && j < UINT_MAX; j--) {
		double q[N];
		for (unsigned i = j + 1; i < N; i++) {
			q[i] = a[j][i];
			a[j][i] = 0;
		}

		for (unsigned i = N - 1; i > j; i--)
			if (q[i] != 0) {
				double s, c;

				if (q[i] < 0) {
					c = q[i] + 1;
					s = -sqrt(1 - c * c);
				}

				else {
					c = q[i] - 1;
					s = sqrt(1 - c * c);
				}

				for (unsigned k = 0; k < N; k++) {
					double jk = a[j][k];

					a[j][k] = c * jk + s * a[i][k];
					a[i][k] = -s * jk + c * a[i][k];
				}
			}

		std::swap(a[j + 1], a[str_transp[j + 1]]);
		swap_str(a, j, col_transp[j], j);
	}
}
*/

double** Q(double **a, int N) {
	double **q = new double*[N];
	for (int i = 0; i < N; i++) {
		q[i] = new double[N];
	}

	double s;
	double c;
	
	double _a_i;
	double _a_j;

	// a[][j] = a_j[i] - Это текущий столбец из матрицы
	// a_j[i]*a_j[i] + a_j[j]*a_j[j] != 0  
	for (int j = 0; j < N; j++) { // j - столбец
		for (int i = j + 1; i < N; i++) { // i - строка

			_a_i = a[i][j]; // - i-ый элемент в столбце векторе a (j-го столбца)
			_a_j = a[j][j]; // - j-ый элемент в столбце векторе a (j-го столбца)

			if (((_a_i * _a_i) + (_a_j * _a_j)) == 0) { // Чтобы небыло деления на 0
				continue;
			}

			double det = sqrt((_a_i * _a_i) + (_a_j * _a_j));
			s = -(_a_i / det);
			c = _a_j / det;

			cout << "++++++++++++++++++++++++++++++++++++++++" << endl;
			cout << s * s + c * c << endl;
			cout << "++++++++++++++++++++++++++++++++++++++++" << endl;

			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++)
					cout << a[i][j] << " ";
				cout << "\n";
			}
			cout << "----------------------------------------" << endl;

			for (int jj = j; jj < N; jj++) {
				double buf = a[j][jj];
				a[j][jj] = c * buf - s * a[i][jj];
				a[i][jj] = s * buf + c * a[i][jj];
			}

			// Теперь нужно запускать обратный обход + добавить сигму в нули выше
		}
	}

	return a;
};


int main() {
	int N = 3;
	double **arr = new double*[N];
	for (int i = 0; i < N; i++) {
		arr[i] = new double[N];
	}
	arr[0][0] = 1;
	arr[0][1] = 3;
	arr[0][2] = 1;

	arr[1][0] = 2;
	arr[1][1] = 1;
	arr[1][2] = 1;

	arr[2][0] = 1;
	arr[2][1] = 4;
	arr[2][2] = 0;

	double **res = Q(arr, N);

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++)
			cout << res[i][j] << " ";
		cout << "\n";
	}

	/*
	double matr[3][3] =   {	1,3,1,
							2,1,1,
							1,4,0, };
	int len = std::size(matr);
	for (int i = 0; i < len; i++) {
		for (int j = 0; j < len; j++)
			cout << matr[i][j] << " ";
		cout << "\n";
	}
	Q(arr);
	*/
}

/*
int sign(double x) {
	return x > 0? 1 : -1;
}


void swap_str(double a[N][N], unsigned str1, unsigned str2, unsigned from) {
	for (unsigned j = from; j < N; j++)
		std::swap(a[j][str1], a[j][str2]);
}


void up_triangularize(double a[N][N], unsigned col_transp[N], unsigned str_transp[N]) {
	for (unsigned j = 0; j < N - 1; j++) {
		if (j < N - 1) {
			double max_len = 0;
			unsigned max_len_col = j;
			for (unsigned i = 0; i < N; i++)
				max_len += a[j][i] * a[j][i];

			for (unsigned k = j + 1; k < N; k++) {
				double len = 0;
				for (unsigned i = 0; i < N; i++)
					len += a[k][i] * a[k][i];

				if (len > max_len) {
					max_len = len;
					max_len_col = k;
				}
			}

			col_transp[j] = max_len_col;
			std::swap(a[j], a[max_len_col]);

			double max_elem = fabs(a[j][j + 1]);
			unsigned max_elem_str = j + 1;
			for (unsigned i = j + 2; i < N; i++)
				if (fabs(a[j][i]) > max_elem) {
					max_elem = fabs(a[j][i]);
					max_elem_str = i;
				}

			str_transp[j + 1] = max_elem_str;
			swap_str(a, j + 1, max_elem_str, j);
		}

		for (unsigned i = j + 1; i < N; i++) {
			double a_i = a[j][i];

			if (a_i != 0) {
				double a_j = a[j][j];
				double z = fmax(fabs(a_j), fabs(a_i));
				double a_l = fmin(fabs(a_j), fabs(a_i)) / z;
				double den = sqrt(1 + a_l * a_l);
				double s = -a_i / z / den;
				double c = a_j / z / den;

				a[j][j] = c * a_j - s * a_i;
				a[j][i] = c + sign(s);

				for (unsigned k = j + 1; k < N; k++) {
					double kj = a[k][j];

					a[k][j] = c * kj - s * a[k][i];
					a[k][i] = s * kj + c * a[k][i];
				}
			}
		}
	}
}


void revert_tr(double a[N][N]) {
	for (unsigned i = N - 1; i >= 0 && i < UINT_MAX; i--) {
		for (unsigned j = N - 1; j >= i && j < UINT_MAX; j--) {
			if (i != j) {
				double ij = 0;
				for (unsigned k = i + 1; k <= j; k++)
					ij += a[k][i] * a[j][k];
				a[j][i] = -1 / a[i][i] * ij;
			}

			else
				a[i][i] = 1 / a[i][i];
		}
	}
}


void de_up_triangularize(double a[N][N], unsigned col_transp[N], unsigned str_transp[N]) {
	for (unsigned j = N - 2; j >= 0 && j < UINT_MAX; j--) {
		double q[N];
		for (unsigned i = j + 1; i < N; i++) {
			q[i] = a[j][i];
			a[j][i] = 0;
		}

		for (unsigned i = N - 1; i > j; i--)
			if (q[i] != 0) {
				double s, c;

				if (q[i] < 0) {
					c = q[i] + 1;
					s = -sqrt(1 - c * c);
				}

				else {
					c = q[i] - 1;
					s = sqrt(1 - c * c);
				}

				for (unsigned k = 0; k < N; k++) {
					double jk = a[j][k];

					a[j][k] = c * jk + s * a[i][k];
					a[i][k] = -s * jk + c * a[i][k];
				}
			}

		std::swap(a[j + 1], a[str_transp[j + 1]]);
		swap_str(a, j, col_transp[j], j);
	}
}


void revert(double a[N][N]) {
	unsigned col_transp[N], str_transp[N];

	up_triangularize(a, col_transp, str_transp);
	revert_tr(a);
	de_up_triangularize(a, col_transp, str_transp);
}
*/